package com.sce.edu.iot.model;

public class Delta {

	private DeviceAttributes desiredAttributes;

	public DeviceAttributes getDesiredAttributes() {
		return desiredAttributes;
	}

	public void setDesiredAttributes(DeviceAttributes desiredAttributes) {
		this.desiredAttributes = desiredAttributes;
	}
}
