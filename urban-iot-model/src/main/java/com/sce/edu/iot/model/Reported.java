package com.sce.edu.iot.model;

import java.util.Map;

public class Reported {

	private Map<String, String> state;

	public Map<String, String> getState() {
		return state;
	}

	public void setState(Map<String, String> state) {
		this.state = state;
	}

}
