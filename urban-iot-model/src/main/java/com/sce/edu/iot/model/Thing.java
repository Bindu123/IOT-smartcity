package com.sce.edu.iot.model;

import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "thing")
public class Thing {

	@Id
	private String id;

	private String topic;
	private DeviceMetadata deviceMetadata;
	private Desired desiredState;
	private List<Reported> reportedState;
	private Delta deltaState;
	private long version;
	private Date timeStamp;

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public DeviceMetadata getDeviceMetadata() {
		return deviceMetadata;
	}

	public void setDeviceMetadata(DeviceMetadata deviceMetadata) {
		this.deviceMetadata = deviceMetadata;
	}

	public Desired getDesiredState() {
		return desiredState;
	}

	public void setDesiredState(Desired desiredState) {
		this.desiredState = desiredState;
	}

	public List<Reported> getReportedState() {
		return reportedState;
	}

	public void setReportedState(List<Reported> reportedState) {
		this.reportedState = reportedState;
	}

	public Delta getDeltaState() {
		return deltaState;
	}

	public void setDeltaState(Delta deltaState) {
		this.deltaState = deltaState;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
}
