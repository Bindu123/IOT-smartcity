package com.sce.edu.iot.model;

import java.util.List;

public class DeviceMetadata {

	private String deviceName;
	private String manufacturer;
	private String type;
	private String serialNumber;
	private List<String> deviceAttributes;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public List<String> getDeviceAttributes() {
		return deviceAttributes;
	}

	public void setDeviceAttributes(List<String> deviceAttributes) {
		this.deviceAttributes = deviceAttributes;
	}

}
