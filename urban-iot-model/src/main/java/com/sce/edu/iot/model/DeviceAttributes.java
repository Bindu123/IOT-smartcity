package com.sce.edu.iot.model;

import java.util.Map;

public class DeviceAttributes {

	private Map<String, String> attributes;

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
}
