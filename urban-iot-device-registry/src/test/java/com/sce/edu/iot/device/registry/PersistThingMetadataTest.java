package com.sce.edu.iot.device.registry;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.device.registry.camel.PersistThingMetadata;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class PersistThingMetadataTest {

	private PersistThingMetadata metadataPersister = null;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SimpleMqttPublisher mqttPublisher;

	private String acceptTopicName = "sce/iot/registry/update/accepted";
	private String rejectTopicName = "sce/iot/registry/update/rejected";

	@Before
	public void setup() {
		cleanup();
		metadataPersister = new PersistThingMetadata();
		metadataPersister.setMongoTemplate(mongoTemplate);
		metadataPersister.setMqttPublisher(mqttPublisher);
		metadataPersister.setAcceptTopicName(acceptTopicName);
		metadataPersister.setRejectTopicName(rejectTopicName);
		metadataPersister.setMainTopicName("sce/iot");
		
	}

	@Test
	public void verifyDeviceRegistry() {
		List<Thing> things = new ArrayList<>();
		things.add(getMockThing());
		metadataPersister.writeThing(things);
		long count = mongoTemplate.count(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
		assertEquals(1, count);
	}

	private Thing getMockThing() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}

	private DeviceMetadata getMockDeviceMetadata() {
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		return deviceMetadata;
	}
	
	//@After
	public void cleanup(){
		mongoTemplate.remove(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
	}

}
