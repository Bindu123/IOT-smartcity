package com.sce.edu.iot.device.registry;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.device.registry.camel.RetreiveThingMetadata;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class RetreiveThingMetadataTest {

	private RetreiveThingMetadata retreiveThing = null;
	
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SimpleMqttPublisher mqttPublisher;

	private String acceptTopicName = "sce/iot/registry/get/accepted";
	private String rejectTopicName = "sce/iot/registry/get/rejected";
	
	private Exchange exchange = null;
	private ObjectMapper mapper = new ObjectMapper();


	@Before
	public void setup() {
		cleanup();
		retreiveThing = new RetreiveThingMetadata();
		retreiveThing.setMongoTemplate(mongoTemplate);
		retreiveThing.setMqttPublisher(mqttPublisher);
		retreiveThing.setAcceptTopicName(acceptTopicName);
		retreiveThing.setRejectTopicName(rejectTopicName);
		
		exchange = mock(Exchange.class);
	}

	@Test
	public void verifyFetchDeviceMetadata() throws JsonProcessingException {
		mongoTemplate.save(getMockThing());
		
		when(exchange.getIn()).thenReturn(mock(Message.class));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(new Thing()));

		retreiveThing.retreiveDeviceMetadata(exchange);
		long count = mongoTemplate.count(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
		assertEquals(1, count);
	}
	
	@Test
	public void verifyFetchDeviceMetadataForEmptyString() throws JsonProcessingException {
		mongoTemplate.save(getMockThing());
		mongoTemplate.save(getMockThing());
		mongoTemplate.save(getMockThing());
		
		when(exchange.getIn()).thenReturn(mock(Message.class));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(getMockThing()));

		retreiveThing.retreiveDeviceMetadata(exchange);
		long count = mongoTemplate.count(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
		assertEquals(3, count);
	}
	
	private Thing getMockThing() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}

	private DeviceMetadata getMockDeviceMetadata() {
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		return deviceMetadata;
	}
	
	@After
	public void cleanup(){
		mongoTemplate.remove(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
	}

}
