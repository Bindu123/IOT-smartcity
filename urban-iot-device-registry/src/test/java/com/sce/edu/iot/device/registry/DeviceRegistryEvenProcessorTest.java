package com.sce.edu.iot.device.registry;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.device.registry.camel.DeviceRegistryEventProcessor;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

public class DeviceRegistryEvenProcessorTest {
	
	private Exchange exchange = null;
	private ObjectMapper mapper = new ObjectMapper();
	private DeviceRegistryEventProcessor registry = null;
	
	@Before
	public void setup(){
		exchange = mock(Exchange.class);
		registry = new DeviceRegistryEventProcessor();
	}
	
	@Test
	public void validateEvent() throws JsonProcessingException{
		
		when(exchange.getIn()).thenReturn(mock(Message.class));
		System.out.println(mapper.writeValueAsString(getMockThing()));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(getMockThing()));
		Thing thing = registry.processDeviceRegistryEvent(exchange);
		assertEquals(thing.getDeviceMetadata().getDeviceName(), getMockDeviceMetadata().getDeviceName());
	}
	
	private Thing getMockThing(){
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}
	
	private DeviceMetadata getMockDeviceMetadata(){
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		List<String> deviceAttributes = new ArrayList<>();
		deviceAttributes.add("current_temp");
		deviceMetadata.setDeviceAttributes(deviceAttributes);
		return deviceMetadata;
	}
}


