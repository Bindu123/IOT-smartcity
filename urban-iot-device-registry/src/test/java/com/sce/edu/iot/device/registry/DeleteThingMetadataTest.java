package com.sce.edu.iot.device.registry;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.device.registry.camel.DeleteThingMetadata;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class DeleteThingMetadataTest {

	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private SimpleMqttPublisher mqttPublisher;

	private String acceptTopicName = "sce/iot/registry/delete/accepted";
	private String rejectTopicName = "sce/iot/registry/delete/rejected";

	private DeleteThingMetadata deleteDevice = null;
	private Exchange exchange = null;

	private ObjectMapper mapper = new ObjectMapper();

	@Before
	public void setup() {
		deleteDevice = new DeleteThingMetadata();
		deleteDevice.setMongoTemplate(mongoTemplate);
		deleteDevice.setMqttPublisher(mqttPublisher);
		deleteDevice.setAcceptTopicName(acceptTopicName);
		deleteDevice.setRejectTopicName(rejectTopicName);
		exchange = mock(Exchange.class);
	}

	@Test
	public void validateDeviceUnregistration() throws JsonProcessingException {
		List<Thing> things = new ArrayList<>();
		things.add(getMockThing());
		mongoTemplate.save(getMockThing());

		when(exchange.getIn()).thenReturn(mock(Message.class));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(getMockThing()));

		deleteDevice.deleteThing(exchange);
		long count = mongoTemplate.count(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
		assertEquals(0, count);
	}

	private Thing getMockThing() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}

	private DeviceMetadata getMockDeviceMetadata() {
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		return deviceMetadata;
	}

}
