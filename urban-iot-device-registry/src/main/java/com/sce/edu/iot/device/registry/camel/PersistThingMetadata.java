package com.sce.edu.iot.device.registry.camel;

import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.model.Thing;

public class PersistThingMetadata {

	private MongoTemplate mongoTemplate;
	private SimpleMqttPublisher mqttPublisher;
	private String mainTopicName;
	private String acceptTopicName;
	private String rejectTopicName;

	@Handler
	public void writeThing(@Body List<Thing> things) {
		try {
			for (Thing thing : things) {
				Query searchMetadataQuery = new Query(
						Criteria.where("deviceMetadata.deviceName").is(thing.getDeviceMetadata().getDeviceName()));
				if (mongoTemplate.exists(searchMetadataQuery, Thing.class)) {
					String response = "Device Name already exist";
					mqttPublisher.publish(rejectTopicName, response.getBytes());
				} else {
					StringBuilder buildTopicName = new StringBuilder();
					buildTopicName.append(getMainTopicName()).append("/").append(thing.getDeviceMetadata().getDeviceName())
							.append("/shadow/update");
					thing.setTopic(buildTopicName.toString());
					mongoTemplate.save(thing);
					String response = "Registration completed successfully";
					mqttPublisher.publish(acceptTopicName, response.getBytes());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			String errorResponse = "Failed to Register the Device";
			mqttPublisher.publish(rejectTopicName, errorResponse.getBytes());
		}
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public SimpleMqttPublisher getMqttPublisher() {
		return mqttPublisher;
	}

	public void setMqttPublisher(SimpleMqttPublisher mqttPublisher) {
		this.mqttPublisher = mqttPublisher;
	}

	public String getMainTopicName() {
		return mainTopicName;
	}

	public void setMainTopicName(String mainTopicName) {
		this.mainTopicName = mainTopicName;
	}

	public String getAcceptTopicName() {
		return acceptTopicName;
	}

	public void setAcceptTopicName(String acceptTopicName) {
		this.acceptTopicName = acceptTopicName;
	}

	public String getRejectTopicName() {
		return rejectTopicName;
	}

	public void setRejectTopicName(String rejectTopicName) {
		this.rejectTopicName = rejectTopicName;
	}

}
