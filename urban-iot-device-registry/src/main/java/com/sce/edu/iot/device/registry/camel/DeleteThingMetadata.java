package com.sce.edu.iot.device.registry.camel;

import java.io.IOException;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.model.Thing;

public class DeleteThingMetadata {

	private static final Logger log = LoggerFactory.getLogger(DeleteThingMetadata.class);
	private static final ObjectMapper mapper;
	private MongoTemplate mongoTemplate;
	private SimpleMqttPublisher mqttPublisher;
	private String acceptTopicName;
	private String rejectTopicName;

	static {
		mapper = new ObjectMapper();
	}

	@Handler
	public void deleteThing(@Body Exchange exchange) {
		String obj = exchange.getIn().getBody(String.class);
		if (obj != null) {
			log.debug("Deserializing " + Thing.class + " MQTT Message");
			try {
				Thing thing = (Thing) mapper.readValue(obj.getBytes(), Thing.class);
				Query searchMetadataQuery = new Query(
						Criteria.where("deviceMetadata.deviceName").is(thing.getDeviceMetadata().getDeviceName()));
				mongoTemplate.remove(searchMetadataQuery, Thing.class);
				String response = "Unregistered successfully";
				mqttPublisher.publish(acceptTopicName, response.getBytes());
			} catch (IOException e) {
				log.warn("Encountered error deserializing MQTT Message", e);
				String errorResponse = "Failed to deserialize message";
				mqttPublisher.publish(rejectTopicName, errorResponse.getBytes());
			}
		} else {
			log.warn("MQTT Message was null!");
			String errorResponse = "Empty Device Name";
			mqttPublisher.publish(rejectTopicName, errorResponse.getBytes());
		}
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public SimpleMqttPublisher getMqttPublisher() {
		return mqttPublisher;
	}

	public void setMqttPublisher(SimpleMqttPublisher mqttPublisher) {
		this.mqttPublisher = mqttPublisher;
	}

	public String getAcceptTopicName() {
		return acceptTopicName;
	}

	public void setAcceptTopicName(String acceptTopicName) {
		this.acceptTopicName = acceptTopicName;
	}

	public String getRejectTopicName() {
		return rejectTopicName;
	}

	public void setRejectTopicName(String rejectTopicName) {
		this.rejectTopicName = rejectTopicName;
	}

}