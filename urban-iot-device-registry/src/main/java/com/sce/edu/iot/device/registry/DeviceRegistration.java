package com.sce.edu.iot.device.registry;

import java.util.ArrayList;
import java.util.List;

import com.sce.edu.iot.common.SimpleMqttClient;
import com.sce.edu.iot.model.DeviceMetadata;

public class DeviceRegistration implements Registration {

	String rejectedTopicName = "sce/iot/register/update/rejected";
	String acceptedTopicName = "sce/iot/register/update/accepted";

	public boolean register(byte[] bytes) {
		DeviceMetadata metadata = deserialize(bytes);
		if (isValidDeviceMetadata(metadata)) {
			persistDeviceMetadata(metadata);
		} else {
			SimpleMqttClient.publishError(rejectedTopicName, "Failed to register Device");
			return false;
		}
		SimpleMqttClient.publishResponse(acceptedTopicName, "Device registration successfull");
		return true;
	}

	public boolean update(byte[] bytes) {
		DeviceMetadata metadata = deserialize(bytes);
		if (isValidDeviceMetadata(metadata)) {
			updateDeviceMetadata(metadata);
		} else {
			SimpleMqttClient.publishError(rejectedTopicName, "Failed to update Device");
			return false;
		}
		SimpleMqttClient.publishResponse(acceptedTopicName, "Device update successfull");
		return true;
	}

	public boolean delete(byte[] bytes) {
		DeviceMetadata metadata = deserialize(bytes);
		if (isValidDeviceMetadata(metadata)) {
			deleteDeviceMetadata(metadata);
		} else {
			SimpleMqttClient.publishError(rejectedTopicName, "Failed to delete Device");
			return false;
		}
		SimpleMqttClient.publishResponse(acceptedTopicName, "Unregistration successfull");
		return true;
	}

	public List<DeviceMetadata> get() {
		return getMetadataFromDB(); 
	}

	private boolean isValidDeviceMetadata(DeviceMetadata metadata) {

		return false;
	}

	private boolean persistDeviceMetadata(DeviceMetadata metadata) {

		return false;
	}
	
	private boolean updateDeviceMetadata(DeviceMetadata metadata) {

		return false;
	}
	
	private boolean deleteDeviceMetadata(DeviceMetadata metadata) {

		return false;
	}
	
	private List<DeviceMetadata> getMetadataFromDB() {
		return new ArrayList<DeviceMetadata>();
	}

	private DeviceMetadata deserialize(byte[] bytes) {

		return null;
	}

}
