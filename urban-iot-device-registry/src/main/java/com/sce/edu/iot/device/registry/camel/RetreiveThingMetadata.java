package com.sce.edu.iot.device.registry.camel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

public class RetreiveThingMetadata {

	private static final Logger log = LoggerFactory.getLogger(RetreiveThingMetadata.class);
	private static final ObjectMapper mapper;
	private MongoTemplate mongoTemplate;
	private SimpleMqttPublisher mqttPublisher;
	private String acceptTopicName;
	private String rejectTopicName;

	static {
		mapper = new ObjectMapper();
	}

	@Handler
	public void retreiveDeviceMetadata(@Body Exchange exchange) {
		String obj = exchange.getIn().getBody(String.class);
		if (obj != null) {
			List<Thing> things = null;
			log.debug("Deserializing " + Thing.class + " MQTT Message");
			try {
				Thing thing = (Thing) mapper.readValue(obj.getBytes(), Thing.class);
				if (thing.getDeviceMetadata() == null) {
					things = mongoTemplate.findAll(Thing.class);
				} else {
					Query searchMetadataQuery = new Query(
							Criteria.where("deviceMetadata.deviceName").is(thing.getDeviceMetadata().getDeviceName()));
					things = mongoTemplate.find(searchMetadataQuery, Thing.class);
				}
				List<DeviceMetadata> deviceMetadataList = new ArrayList<>();
				for(Thing dbthing : things){
					deviceMetadataList.add(dbthing.getDeviceMetadata());
				}
				
				mqttPublisher.publish(acceptTopicName, mapper.writeValueAsBytes(deviceMetadataList));
			} catch (IOException e) {
				log.warn("Encountered error deserializing MQTT Message", e);
				String errorResponse = "Encountered error deserializing MQTT Message";
				mqttPublisher.publish(rejectTopicName, errorResponse.getBytes());
			}
		} else {
			log.warn("MQTT Message was null!");
			String errorResponse = "Failed to fetch registered device information";
			mqttPublisher.publish(rejectTopicName, errorResponse.getBytes());
		}
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public SimpleMqttPublisher getMqttPublisher() {
		return mqttPublisher;
	}

	public void setMqttPublisher(SimpleMqttPublisher mqttPublisher) {
		this.mqttPublisher = mqttPublisher;
	}

	public String getAcceptTopicName() {
		return acceptTopicName;
	}

	public void setAcceptTopicName(String acceptTopicName) {
		this.acceptTopicName = acceptTopicName;
	}

	public String getRejectTopicName() {
		return rejectTopicName;
	}

	public void setRejectTopicName(String rejectTopicName) {
		this.rejectTopicName = rejectTopicName;
	}

}