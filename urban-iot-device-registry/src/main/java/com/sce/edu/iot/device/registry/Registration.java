package com.sce.edu.iot.device.registry;

import java.util.List;

import com.sce.edu.iot.model.DeviceMetadata;

public interface Registration {
	
	public boolean register(byte[] bytes);
	
	public boolean update(byte[] bytes);
	
	public boolean delete(byte[] bytes);
	
	public List<DeviceMetadata> get();

}
