package com.sce.edu.iot.device.registry;

import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeviceRegistrationMain {

	private static final Logger log = LoggerFactory.getLogger(DeviceRegistrationMain.class);
	
	public static void main(String[] args) {
		
		log.info("Starting Device Registration Module");
		try {
            if ((args == null) || (args.length == 0)) {
                Main.main("-fileApplicationContext", "file:${conf.loc}/applicationContext.xml");
            }
            else {
                Main.main(args);
            }
        }
        catch (final Exception e) {
        	log.error("Caught exception starting Camel!", e);
        }
		log.info("Process Exiting");
	}
		
//		ApplicationContext ctx = new GenericXmlApplicationContext("SpringConfig.xml");
//		MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
//		
		
//			// save
//			mongoOperation.save(user);
//
//			// now user object got the created id.
//			System.out.println("1. user : " + user);
//
//			// query to search user
//			Query searchUserQuery = new Query(Criteria.where("username").is("mkyong"));
//
//			// find the saved user again.
//			User savedUser = mongoOperation.findOne(searchUserQuery, User.class);
//			System.out.println("2. find - savedUser : " + savedUser);
//
//			// update password
//			mongoOperation.updateFirst(searchUserQuery, Update.update("password", "new password"),
//					User.class);
//
//			// find the updated user object
//			User updatedUser = mongoOperation.findOne(
//					new Query(Criteria.where("username").is("mkyong")), User.class);
//
//			System.out.println("3. updatedUser : " + updatedUser);
//
//			// delete
//			mongoOperation.remove(searchUserQuery, User.class);
//
//			// List, it should be empty now.
//			List<User> listUser = mongoOperation.findAll(User.class);
//			System.out.println("4. Number of user = " + listUser.size());

//		}

}
