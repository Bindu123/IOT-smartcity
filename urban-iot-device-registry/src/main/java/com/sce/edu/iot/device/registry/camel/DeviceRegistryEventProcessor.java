package com.sce.edu.iot.device.registry.camel;

import java.io.IOException;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.JacksonAnnotationIntrospector;
import com.sce.edu.iot.model.Thing;

public class DeviceRegistryEventProcessor {

	    private static final Logger log	= LoggerFactory.getLogger(DeviceRegistryEventProcessor.class);
	    private static final ObjectMapper mapper;

	    static {
	        mapper = new ObjectMapper();
//	        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
//	        mapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, true);
//	        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
//	        mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
//	        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//	        mapper.setAnnotationIntrospector(new JacksonAnnotationIntrospector());	    
	    }
	    
	    @Handler
	    public Thing processDeviceRegistryEvent(@Body Exchange exchange) {
	        String obj = exchange.getIn().getBody(String.class);
	        if (obj != null) {
	            log.debug("Deserializing " + Thing.class + " MQTT Message");
	            try {
	            	return (Thing) mapper.readValue(obj.getBytes(), Thing.class);
	            }
	            catch (IOException e) {
	                log.warn("Encountered error deserializing MQTT Message", e);
	                return null;
	            }

	        }
	        else {
	            log.warn("MQTT Message was null!");
	            return null;
	        }
	    }
}