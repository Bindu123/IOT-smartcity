Internet of things shall be able to incorporate transparently and seamlessly a large number of different and heterogeneous end systems, while 
providing open access to selected subsets of data for the development of plethora of digital services. Here focus is specifically to 
urban IoT systems that, while still being quite a broad category, are characterized by their specific application domain. 
This project proposes to use MQTT protocol and software simulators. Its a many-to-many communication protocol for passing messages between multiple clients through a central broker.

Software Requirements:

Languages : Java, JavaScript, HTML5, CSS

Cloud Infrastucture : Amazon Web Services

IDE : Eclipse

Database : MongoDB

Dependency Management : MAven

Protocols : MQTT, WebSocket


System contains:

1.Dashboard

2.Virtual Device

3.Message Broker

4.Device Shadow

5.Device Registry

6.Data Store


Working of the System

First enter all the required information about the device such as device name, device type,etc. And all these information will be sent to device registry through 
message broker since device registry will be subscribed to message broker. While data will be in the form of bytes and this will be converted into java object i.e serialization happens.
If entered information is valid then it is stored in data store. If entered information is invalid, then it will be notifies on dashboard through message broker. After devicec registration
is done then virtual device will start simulation. Virtual device will start sending events to device shadow through message broker, since device shadow is subscribed to message broke.
Those events will be converted and validated by device shadow and stored into data store.
