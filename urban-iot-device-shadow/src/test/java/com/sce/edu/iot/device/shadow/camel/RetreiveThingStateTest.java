package com.sce.edu.iot.device.shadow.camel;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Reported;
import com.sce.edu.iot.model.Thing;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class RetreiveThingStateTest {

	private Exchange exchange = null;
	private ObjectMapper mapper = new ObjectMapper();
	private RetreiveThingState retreive = null;
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private SimpleMqttPublisher mqttPublisher;


	@Before
	public void setup() {
		cleanup();
		exchange = mock(Exchange.class);
		retreive = new RetreiveThingState();
		retreive.setMongoTemplate(mongoTemplate);
		retreive.setMqttPublisher(mqttPublisher);
		retreive.setMainTopicName("sce/iot");
	}

	@Test
	public void verifygetThingShadowReportedState() throws JsonProcessingException {
		//store thing state
		mongoTemplate.save(getMockThingState());
		
		when(exchange.getIn()).thenReturn(mock(Message.class));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(getMockThing()));
		retreive.retreiveThingState(exchange);
		//Verify
		Query query = new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1"));
		query.fields().elemMatch("reportedState.reportedAttributes.attributes", Criteria.where("temp").is("50"));
		long count = mongoTemplate.count(query, "thing");
		assertEquals(1, count);
	}
	
	private Thing getMockThingState() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		Reported reportedState = new Reported();
		Map<String, String> attributes = new HashMap<>();
		attributes.put("temp", "100");
		reportedState.setState(attributes);
		thing.setReportedState(Arrays.asList(reportedState));
		return thing;
	}
	
	private Thing getMockThing() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}

	private DeviceMetadata getMockDeviceMetadata() {
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		return deviceMetadata;
	}
	
	@After
	public void cleanup(){
		mongoTemplate.remove(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
	}

}
