package com.sce.edu.iot.device.shadow.camel;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Reported;
import com.sce.edu.iot.model.Thing;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class DeviceShadowPersisterTest {

	private DeviceShadowPersister shadowPersister = null;
	
	@Autowired
	private MongoTemplate mongoTemplate;


	@Before
	public void setup() {
		cleanup();
		shadowPersister = new DeviceShadowPersister();
		shadowPersister.setMongoTemplate(mongoTemplate);
	}

	@Test
	public void verifyDeviceShadowPersister() {
		//Register
		mongoTemplate.save(getMockThing());

		//Insert first state
		List<Thing> thingStates = new ArrayList<>();
		thingStates.add(getFirstMockThingState());
		shadowPersister.writeThingState(thingStates);
		
		//Verify
		Query query = new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1"));
		query.fields().elemMatch("reportedState.reportedAttributes.attributes", Criteria.where("temp").is("50"));
		long count = mongoTemplate.count(query, "thing");
		assertEquals(1, count);
	}
	
	@Test
	public void verifyDeviceMultiShadowPersister() {
		//Register
		mongoTemplate.save(getMockThing());
		
		// Insert first State
		shadowPersister.writeThingState(Arrays.asList(getFirstMockThingState()));
		//Insert second state
		shadowPersister.writeThingState(Arrays.asList(getSecondMockThingState()));

		Query query = new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1"));
		query.fields().elemMatch("reportedState.reportedAttributes.attributes", Criteria.where("temp").is("100"));
		long count = mongoTemplate.count(query, "thing");
		assertEquals(1, count);
	}
	
	private Thing getSecondMockThingState() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		Reported reportedState = new Reported();
		Map<String, String> attributes = new HashMap<>();
		attributes.put("temp", "100");
		reportedState.setState(attributes);
		thing.setReportedState(Arrays.asList(reportedState));
		return thing;
	}
	
	private Thing getFirstMockThingState(){
		Thing thing = getMockThing();
		Reported reportedState = new Reported();
		Map<String, String> attributes = new HashMap<>();
		attributes.put("temp", "50");
		reportedState.setState(attributes);
		thing.setReportedState(Arrays.asList(reportedState));
		return thing;
	}

	private Thing getMockThing() {
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		return thing;
	}

	private DeviceMetadata getMockDeviceMetadata() {
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		return deviceMetadata;
	}
	
	@After
	public void cleanup(){
		mongoTemplate.remove(new Query(Criteria.where("deviceMetadata.deviceName").is("Sensor1")), "thing");
	}

}
