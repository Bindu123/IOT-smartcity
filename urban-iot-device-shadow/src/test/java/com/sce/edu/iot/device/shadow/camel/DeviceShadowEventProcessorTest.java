package com.sce.edu.iot.device.shadow.camel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Reported;
import com.sce.edu.iot.model.Thing;

public class DeviceShadowEventProcessorTest {

	private Exchange exchange = null;
	private ObjectMapper mapper = new ObjectMapper();
	private DeviceShadowEventProcessor shadowProcessor = null;
	
	@Before
	public void setup(){
		exchange = mock(Exchange.class);
		shadowProcessor = new DeviceShadowEventProcessor();
	}
	
	@Test
	public void validateEvent() throws JsonProcessingException{
		
		when(exchange.getIn()).thenReturn(mock(Message.class));
		System.out.println(mapper.writeValueAsString(getMockThing()));
		when(exchange.getIn().getBody(String.class)).thenReturn(mapper.writeValueAsString(getMockThing()));
		Thing thing = shadowProcessor.processDeviceShadowEvent(exchange);
		assertEquals(thing.getDeviceMetadata().getDeviceName(), getMockDeviceMetadata().getDeviceName());
		assertEquals(thing.getReportedState().get(0).getState().get("current_temp"), "50");
	}
	
	private Thing getMockThing(){
		Thing thing = new Thing();
		thing.setDeviceMetadata(getMockDeviceMetadata());
		Map<String, String> state = new HashMap<>();
		state.put("current_temp", "50");
		Reported reportedState = new Reported();
		reportedState.setState(state);
		thing.setReportedState(Arrays.asList(reportedState));
		return thing;
	}
	
	private DeviceMetadata getMockDeviceMetadata(){
		DeviceMetadata deviceMetadata = new DeviceMetadata();
		deviceMetadata.setDeviceName("Sensor1");
		deviceMetadata.setManufacturer("SCE");
		deviceMetadata.setSerialNumber("1N345678910");
		List<String> deviceAttributes = new ArrayList<>();
		deviceAttributes.add("current_temp");
		deviceMetadata.setDeviceAttributes(deviceAttributes);
		return deviceMetadata;
	}
	
}
