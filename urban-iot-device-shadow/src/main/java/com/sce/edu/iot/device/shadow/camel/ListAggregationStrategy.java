package com.sce.edu.iot.device.shadow.camel;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class ListAggregationStrategy implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		Exchange retVal;

		List list;
		if (oldExchange == null) {
			if (newExchange == null) {
				return null;
			}
			retVal = newExchange;
			list = new ArrayList();
		}
		else {
			retVal = oldExchange;
			list = (List) oldExchange.getIn().getBody();
		}

		Object body = newExchange.getIn().getBody();

		if (body != null) {
			if (body instanceof List) {
				list.addAll((List) body);
			}
			else {
				list.add(body);
			}
		}

		retVal.getIn().setBody(list);

		return retVal;
	}
}
