package com.sce.edu.iot.device.shadow;

import com.sce.edu.iot.common.SimpleMqttClient;
import com.sce.edu.iot.model.Thing;

public class ThingShadow implements Shadow {

	private String errorTopicName = "com/sce/thingShadow/update/error";
	private String responseTopicName = "com/sce/thingShadow/update/response";
	private String deltaTopicName = "com/sce/thingShadow/update/delta";

public boolean saveReportedState(byte[] bytes) {
	Thing Thing = deserializeState(bytes);
	if (isValidState(Thing)) {
		saveState(Thing);
	} else {
		SimpleMqttClient.publishError(errorTopicName, "Failed to deserialize reported state");
		return false;
	}
	SimpleMqttClient.publishResponse(responseTopicName, "Reported State stored successfully");
	return true;
}

public boolean deleteShadow(byte[] bytes) {
	Thing Thing = deserializeState(bytes);
	if (isValidState(Thing)) {
		saveState(Thing);
	} else {
		SimpleMqttClient.publishError(errorTopicName, "Failed to delete shadow");
		return false;
	}
	SimpleMqttClient.publishResponse(responseTopicName, "Shadow deleted");
	return true;
}

public boolean setDesiredState(byte[] bytes) {
	Thing Thing = deserializeState(bytes);
	if (isValidState(Thing)) {
		manageVersion(Thing);
		persist(Thing);
	} else {
		SimpleMqttClient.publishError(errorTopicName, "Failed to set desired state");
		return false;
	}
	SimpleMqttClient.publishResponse(responseTopicName, "Setting desired state successful");
	return true;
}

	public void calculateDelta(byte[] bytes1, byte[] bytes2) {
		Thing thingState1 = deserializeState(bytes1);
		Thing thingState2 = deserializeState(bytes1);
		Thing delta =  diff(thingState1, thingState2);
		SimpleMqttClient.publishResponse(deltaTopicName, delta.toString());
	}

	private Thing diff(Thing thingState1, Thing thingState2) {
		return null;
	}

	private void persist(Thing thing) {
		
	}

	private void manageVersion(Thing thing) {
	}

	
	private Thing deserializeState(byte[] bytes) {
		return new Thing();
	}

	private boolean isValidState(Thing thing) {

		return false;
	}

	private boolean saveState(Thing thing) {

		return false;
	}
	
}
