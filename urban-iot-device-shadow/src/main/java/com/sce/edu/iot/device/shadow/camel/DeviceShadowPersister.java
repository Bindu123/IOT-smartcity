package com.sce.edu.iot.device.shadow.camel;

import java.util.List;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.sce.edu.iot.model.Reported;
import com.sce.edu.iot.model.Thing;

public class DeviceShadowPersister {

	private MongoTemplate mongoTemplate;

	@Handler
	public void writeThingState(@Body List<Thing> thingStates) {
		try {
			for (Thing thing : thingStates) {
				
				Query searchMetadataQuery = new Query(
						Criteria.where("deviceMetadata.deviceName").is(thing.getDeviceMetadata().getDeviceName()));
				
				Update updateState = new Update().inc("version", 1).pushAll("reportedState", thing.getReportedState().toArray(new Reported[thing.getReportedState().size()]));
				Thing thingy = mongoTemplate.findAndModify(searchMetadataQuery, updateState, Thing.class);
				//System.out.println(thingy);
				//mongoTemplate.save(thing);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

}
