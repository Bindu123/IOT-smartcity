package com.sce.edu.iot.device.shadow;

public interface Shadow {

	public boolean saveReportedState(byte[] bytes);

	public boolean deleteShadow(byte[] bytes);

	public boolean setDesiredState(byte[] bytes);

	public void calculateDelta(byte[] bytes1, byte[] bytes2);
}
