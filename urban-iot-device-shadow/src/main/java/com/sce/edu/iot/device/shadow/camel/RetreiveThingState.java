package com.sce.edu.iot.device.shadow.camel;

import java.io.IOException;

import org.apache.camel.Body;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sce.edu.iot.common.SimpleMqttPublisher;
import com.sce.edu.iot.model.DeviceMetadata;
import com.sce.edu.iot.model.Thing;

public class RetreiveThingState {

	private static final Logger log = LoggerFactory.getLogger(RetreiveThingState.class);
	private static final ObjectMapper mapper;
	private MongoTemplate mongoTemplate;
	private SimpleMqttPublisher mqttPublisher;
	private String mainTopicName;

	static {
		mapper = new ObjectMapper();
	}

	@Handler
	public void retreiveThingState(@Body Exchange exchange) {
		String obj = exchange.getIn().getBody(String.class);
		if (obj != null) {
			log.debug("Deserializing " + Thing.class + " MQTT Message");
			try {
				Thing thing = (Thing) mapper.readValue(obj.getBytes(), Thing.class);
				Query searchMetadataQuery = new Query(
						Criteria.where("deviceMetadata.deviceName").is(thing.getDeviceMetadata().getDeviceName()));
				Thing retrievedThing = mongoTemplate.findOne(searchMetadataQuery, Thing.class);
				String publishDeviceStateTopicName = getPublishDeviceStateTopicName(retrievedThing);
				
				Thing publishThingObj = new Thing();
				DeviceMetadata metadata = new DeviceMetadata();
				metadata.setDeviceName(retrievedThing.getDeviceMetadata().getDeviceName());
				publishThingObj.setDeviceMetadata(metadata);
				publishThingObj.setReportedState(retrievedThing.getReportedState());

				mqttPublisher.publish(publishDeviceStateTopicName, mapper.writeValueAsBytes(publishThingObj));
			} catch (IOException e) {
				log.warn("Encountered error deserializing MQTT Message", e);
			}
		} else {
			log.warn("MQTT Message was null!");
		}
	}

	private String getPublishDeviceStateTopicName(Thing thing) {
		StringBuilder topicNameBuilder = new StringBuilder();
		topicNameBuilder.append(mainTopicName).append("/").append(thing.getDeviceMetadata().getDeviceName())
				.append("/shadow/get/response");
		return topicNameBuilder.toString();
	}

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	public SimpleMqttPublisher getMqttPublisher() {
		return mqttPublisher;
	}

	public void setMqttPublisher(SimpleMqttPublisher mqttPublisher) {
		this.mqttPublisher = mqttPublisher;
	}

	public String getMainTopicName() {
		return mainTopicName;
	}

	public void setMainTopicName(String mainTopicName) {
		this.mainTopicName = mainTopicName;
	}

}