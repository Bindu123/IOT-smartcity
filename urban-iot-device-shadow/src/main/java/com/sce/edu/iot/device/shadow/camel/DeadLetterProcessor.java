package com.sce.edu.iot.device.shadow.camel;

import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.Handler;
import org.apache.camel.Headers;
import org.apache.camel.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * If an exception is thrown on a Camel route, the message and the exception are passed here.
 */
public class DeadLetterProcessor {

	private static final Logger log	= LoggerFactory.getLogger(DeadLetterProcessor.class);

	/**
	 * Camel entry point to process the exception. Logs exception and attempts to trace.
	 * 
	 * @param ignored
	 *            message body
	 * @param headers
	 *            camel headers
	 * @param properties
	 *            camel properties
	 */
	@Handler
	public void process(@Body Object ignored, @Headers Map<String, Object> headers, @Properties Map properties) {
		try {

			Exception e = (Exception) properties.get("CamelExceptionCaught");

			if (e == null) {
				log.error("UNKNOWN ERROR in Camel route!");
			}
			else if (e.getMessage() != null) {
				log.error("Exception caught in Camel route - " + e.getMessage(), e);
			}
			else {
				log.error("Exception caught in Camel route", e);
			}
		}
		catch (Exception e) {
			log.error("ERROR processing error in main Camel route!", e);
		}
	}
}
