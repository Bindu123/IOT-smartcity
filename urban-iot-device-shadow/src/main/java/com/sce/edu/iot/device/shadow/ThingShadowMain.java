package com.sce.edu.iot.device.shadow;

import org.apache.camel.spring.Main;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ThingShadowMain {

private static final Logger log = LoggerFactory.getLogger(ThingShadowMain.class);
	
	public static void main(String[] args) {
		
		log.info("Starting Device Shadow Module");
		try {
            if ((args == null) || (args.length == 0)) {
                Main.main("-fileApplicationContext", "file:${conf.loc}/applicationContext.xml");
            }
            else {
                Main.main(args);
            }
        }
        catch (final Exception e) {
        	log.error("Caught exception starting Camel!", e);
        }
		log.info("Process Exiting");
	}

}
