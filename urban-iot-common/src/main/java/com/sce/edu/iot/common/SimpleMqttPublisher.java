package com.sce.edu.iot.common;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

public class SimpleMqttPublisher implements MqttCallback {

	private MqttClient mqttClient = null;

	public SimpleMqttPublisher(String borkerURL, String clientID) throws MqttException {
		MqttConnectOptions connOpt = new MqttConnectOptions();
		connOpt.setCleanSession(true);
		connOpt.setKeepAliveInterval(30);
		mqttClient = new MqttClient(borkerURL, clientID);
		mqttClient.setCallback(this);
		mqttClient.connect(connOpt);
		System.out.println("Connected!!!!");
	}

	public boolean publish(String topicName, byte[] payload) {
		MqttTopic topic = mqttClient.getTopic(topicName);
		MqttMessage mqttMsg = new MqttMessage(payload);
		mqttMsg.setQos(0);
		mqttMsg.setRetained(false);
		MqttDeliveryToken token = null;
		try {
			// publish message to broker
			token = topic.publish(mqttMsg);
			// Wait until the message has been delivered to the broker
			token.waitForCompletion();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public void connectionLost(Throwable arg0) {

	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken arg0) {

	}

	@Override
	public void messageArrived(String arg0, MqttMessage arg1) throws Exception {

	}
}
