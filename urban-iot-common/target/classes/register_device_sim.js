/**
 * http://usejsdoc.org/
 */
var Thread = Java.type("java.lang.Thread");
var System = Java.type("java.lang.System");

function execute(action) {
	out("Executing Script: "+ action.getName());
	for(var i = 0; i < 5; i++){
		increase();
		Thread.sleep(500);
		decrease();
	}
	
	action.setExitCode(0);
	action.setResultText("Done");
	return action;
}

function increase() {
	for(var i = 0; i <= 11 ; i++){
		mqttManager.publish("sce/iot/outdoor/", i);
		out("sce/iot/outdoor/:" +i)
		Thread.sleep(500);
	}
}

function decrease(){
	for(var i = 11; i > -1 ; i--){
		mqttManager.publish("sce/iot/outdoor/", i);
		out("sce/iot/outdoor/:" +i)
		Thread.sleep(500);
	}
}

function out(message){
	output.print(message);
}